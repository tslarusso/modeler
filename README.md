# Description in a few days

[![Build Status](https://travis-ci.org/Larusso/modeler.png?branch=develop)](https://travis-ci.org/Larusso/modeler)    

## License

Copyright (C) 2012 Manfred Endres

Distributed under the Eclipse Public License, the same as Clojure.